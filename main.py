import argparse
import multiprocessing
import os
import uuid

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from tqdm import tqdm

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

TIMEOUT = 4
PROCESS_COUNT = 10


class Server(object):
    def __init__(self, url):
        self.url = url
        self.session = Session()
        self.normal_resp = self.session.get(url)
        self.random_resp = self.session.get(url, host=str(uuid.uuid4()))

    def get_host(self, host):
        resp = self.session.get(self.url, host=host)
        resp.is_interesting = self.is_interesting(resp)
        return resp

    def is_interesting(self, response):
        assert response.host is not None
        if response.error:
            return False

        trivial_response = self.session.get("http://"+response.host)
        trivial_response_s = self.session.get("https://"+response.host)
        return (response != trivial_response and
                response != trivial_response_s and
                response != self.normal_resp and
                response != self.random_resp)


class Session(object):
    def __init__(self):
        self._session = requests.Session()

    def get(self, *args, **kwargs):
        return Response(*args, **kwargs, session=self._session)


class Response(object):
    def __init__(self, url, host=None, session=None):
        self.host = host
        self.resp = None
        self.error = False

        if session is None:
            session = requests.Session()

        headers = {}
        if host is not None:
            headers['Host'] = host

        try:
            self.resp = session.get(url, headers=headers, verify=False, allow_redirects=False, timeout=TIMEOUT)
        except Exception:
            self.error = True

    def __eq__(self, other):
        if self.error or other.error:
            return self.error and other.error

        if self.resp.is_redirect or other.resp.is_redirect:
            return self.resp.headers.get('location') == other.resp.headers.get('location')

        return (self.resp.status_code == other.resp.status_code and
                self.resp.text == other.resp.text)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', required=True)
    parser.add_argument('-f', '--hosts-file', type=argparse.FileType('r'), required=True)
    parser.add_argument('-o', '--out', default="out")

    args = parser.parse_args()

    hosts = [h.strip() for h in args.hosts_file.readlines()]

    server = Server(args.url)

    responses = []
    with multiprocessing.Pool(PROCESS_COUNT) as pool:
        response_iter = pool.imap_unordered(server.get_host, hosts)
        progress_bar = tqdm(response_iter, total=len(hosts), desc="Testing hosts...", unit="host")
        for response in progress_bar:
            if response.is_interesting:
                responses.append(response)

    print()
    print("Tested {} hosts, out of which {} seem interesting".format(len(hosts), len(responses)))

    for r in sorted(responses, key=lambda r: r.resp.status_code):
        print("{} : {} {}".format(r.host, r.resp.status_code, r.resp.reason or ""))
        if r.resp.status_code == 200:
            os.makedirs(args.out, exist_ok=True)
            filename = os.path.join(args.out, r.host+'.html')
            with open(filename, 'w') as f:
                f.write(r.resp.text)
                print("\t==> Check '{}'".format(filename))
